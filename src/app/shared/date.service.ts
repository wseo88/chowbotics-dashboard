import { Injectable, Inject } from "@angular/core";

@Injectable()
export class DateService {
  	current: Date;

    constructor() {
		  this.current = new Date();
  	}

  	now(): Date {
  		return this.current;
  	}

  	today(): Date {
  		let today = this.current;
  		today.setHours(0,0,0,0);
  		return today;
  	}

  	yesterday(): Date {
  		return this.getDaysAgo(1);
  	}

    getDaysAgo(day: number): Date {
      let pastDate = new Date();
      pastDate.setDate(this.today().getDate() - day);
      pastDate.setHours(0,0,0,0);
      return pastDate;
    }

    getNextDay(currentDate: Date): Date {
      let nextDay = new Date();
      nextDay.setDate(currentDate.getDate() + 1);
      nextDay.setHours(0,0,0,0);
      return nextDay;
    }

    convertDateStringToDate(dateString: string): Date {
      let dateStringSplit: string[] = dateString.split('-');
      let timestamp: Date = new Date(Number(dateStringSplit[0]), Number(dateStringSplit[1])-1, Number(dateStringSplit[2]));
      return timestamp;
    }
}
