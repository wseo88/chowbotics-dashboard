import { Injectable, Inject } from "@angular/core";

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { Observable, Subject, BehaviorSubject } from "rxjs";
import * as firebase from 'firebase/app';

@Injectable()
export class SaladDispensingDetailsService {

	saladDispensingDetailsList: FirebaseListObservable<any>;
	saladDispensingDetailsData: any[];

	constructor(public af: AngularFireDatabase) {
		let saladDispensingDetailsListFlattened: any[] = []

    	this.saladDispensingDetailsList = af.list('/casabots/sally001/saladDetails');
    	this.saladDispensingDetailsList
		    .subscribe(snapshots=>{
	        	snapshots.forEach(salad => {
	        		let saladKey: string = salad.$key;

			        let saladDateSplit: string[] = saladKey.split('-');
			        let year = Number(saladDateSplit[0]);
			        let month = Number(saladDateSplit[1]);
			        let day = Number(saladDateSplit[2]);

			        let saladDate: Date = new Date(year, month-1, day);

	        		Object.keys(salad).forEach(codeKey=> {
						let attempts: any[] = salad[codeKey];

						Object.keys(attempts).forEach(attemptKey => {

							let steps: any[] = attempts[attemptKey];

							Object.keys(steps).forEach(gcodeCommand => {

								let instruction: any[] = steps[gcodeCommand];
								let instructionKey = saladKey; //this.generatePushID();

								let temp: {} = {
									"saladKey": saladKey,
									"codeKey": codeKey,
									"attemptKey": attemptKey,
									"gcodeCommand": gcodeCommand,
									"saladDate": saladDate
								};

								Object.keys(steps).forEach(name=>{
									temp[name] = steps[name];
								});

								saladDispensingDetailsListFlattened[instructionKey] = temp;

							});
						});
					});
        		});
        	});

        this.saladDispensingDetailsData = saladDispensingDetailsListFlattened;

  	}

  	getSaladDispensingDetailsList(): FirebaseListObservable<any> {
  		return this.saladDispensingDetailsList;
    }

    getSaladDispensingDetailsData() {
    	return this.saladDispensingDetailsData;
    }
}
