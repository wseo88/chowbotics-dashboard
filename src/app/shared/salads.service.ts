import { Injectable, Inject } from "@angular/core";

import { AngularFireDatabase, FirebaseListObservable, FirebaseObjectObservable } from 'angularfire2/database';
import { Observable, Subject, BehaviorSubject } from "rxjs";
import * as firebase from 'firebase/app';

@Injectable()
export class SaladsService {

    saladsList: FirebaseListObservable<any>;
    saladsData: {};

    constructor(public af: AngularFireDatabase) { }

  	getAll(): FirebaseListObservable<any> {
    	return this.queryData({});
  	}

  	getByDate(startTime, endTime): FirebaseListObservable<any> {
  		let query: {} = {"startAt":startTime, "endAt":endTime, "orderByKey": true}

    	return this.queryData(query);
  	}

  	queryData(query): FirebaseListObservable<any> {

  		if (query == null){
  			query = {};
  		}

    	this.saladsList = this.af.list('/casabots/sally001/salads', {"query": query});
    	this.saladsList.subscribe(snapshots=>{
	        	this.saladsData = this.flattenList(snapshots);
        	});

    	return this.saladsList;

  	}

  	getFlattenedData() {
  		return this.saladsData;
    }

    flattenList(list): {}{
    	let flattenedList: {} = {};
    	list.forEach(salad => {
	        		let saladKey: string = salad.$key;

			        let saladDateSplit: string[] = saladKey.split('-');
			        let year = Number(saladDateSplit[0]);
			        let month = Number(saladDateSplit[1]);
			        let day = Number(saladDateSplit[2]);

			        let saladDate: Date = new Date(year, month-1, day);

					let temp: {} = {
									"saladKey": saladKey,
									"saladDate": saladDate
								};

					Object.keys(salad).forEach(name=>{
									temp[name] = salad[name];
					});

					flattenedList[saladKey] = temp;

        		});

    	
    	return flattenedList;
    }

}
