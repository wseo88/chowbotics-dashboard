import { Input, Component, OnInit, OnChanges } from '@angular/core';
import { SaladsService } from 'app/shared/salads.service';
import { DateService } from 'app/shared/date.service';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-stats-cards',
  templateUrl: './stats-cards.component.html',
  styleUrls: ['./stats-cards.component.scss']
})

export class StatsCardsComponent implements OnInit, OnChanges {

  @Input() saladsList: any[];
  saladsSoldToday: number = 0;

  ngOnInit() { }

  constructor(private saladsService: SaladsService, private dateService: DateService) { }

  ngOnChanges() {
  	if (this.saladsList) {
      this.saladsSoldToday = this.saladsList.length;
  	}
  }
}
