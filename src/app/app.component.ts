import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { AuthService } from "app/shared/auth.service";
import { Observable, BehaviorSubject } from "rxjs";
import {UserInfo} from "app/shared/user-info";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  private alertType = null;
  private alertMessage = "";
  private isLoggedIn = new BehaviorSubject<boolean>(false);

  constructor(public location: Location, private authService: AuthService, private router: Router) {
    this.authService.isLoggedIn().subscribe(this.isLoggedIn);
  }

  currentUser(): Observable<UserInfo> {
    return this.authService.currentUser();
  }

  ngOnInit() {
  }

  isMaps(path){
    var titlee = this.location.prepareExternalUrl(this.location.path());
    titlee = titlee.slice( 1 );
    if(path == titlee){
      return false;
    }
    else {
      return true;
    }
  }

  logout() {
      this.authService.logout();
      this.router.navigate(['/']);
  }

  onLoginSuccess() {
      this.alertType = "success";
      this.alertMessage = "Login Success!";
  }

  onError(err) {
      this.alertType = "danger";
      this.alertMessage = err;
  }

  onLoggedOut() {
      // Just reset any displayed messsage.
      this.alertType = null;
      this.alertMessage = "";
  }

  alertClosed() {
      this.alertType = null;
      this.alertMessage = "";
  }

  navigateToHome() {
      this.router.navigate(['/']);
  }

  navigateToDashboard() {
    this.router.navigate(['/dashboard']);
  }
}
