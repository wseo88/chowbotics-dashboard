import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppRoutingModule } from './app.routing';
import { ComponentsModule } from './components/components.module';

import { AppComponent } from './app.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { TableListComponent } from './table-list/table-list.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { MapsComponent } from './maps/maps.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { HomeComponent } from "./home/home.component";
import { LoginUserComponent } from "app/login-user/login-user.component";
import { DisplayUserComponent } from "app/display-user/display-user.component";

import { firebaseConfig } from "environments/firebaseConfig";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AlertModule } from "ngx-bootstrap";

// Service
import { AuthService } from "app/shared/auth.service";
import { SaladDispensingDetailsService } from 'app/shared/salad-dispensing-details.service';
import { SaladsService } from 'app/shared/salads.service';
import { DateService } from 'app/shared/date.service';

// Charts
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SaladCountChartComponent } from './charts/salad-count-chart/salad-count-chart.component';
import { SaladTimeChartComponent } from './charts/salad-time-chart/salad-time-chart.component';
import { StatisticalTimeChartComponent } from './charts/statistical-time-chart/statistical-time-chart.component';
import { StatisticalIngredientChartComponent } from './charts/statistical-ingredient-chart/statistical-ingredient-chart.component';
import { StatisticalIngredientTimeChartComponent } from './charts/statistical-ingredient-time-chart/statistical-ingredient-time-chart.component';
import { StatsCardsComponent } from './dashboard/stats-cards/stats-cards.component';
import { IngredientsDispenseChartComponent } from './charts/ingredients-dispense-chart/ingredients-dispense-chart.component';
import { IngredientsWeightAccuracyChartComponent } from './charts/ingredients-weight-accuracy-chart/ingredients-weight-accuracy-chart.component';
import { IngredientTimeBubbleChartComponent } from './charts/ingredient-time-bubble-chart/ingredient-time-bubble-chart.component';
import { IngredientsDispenseTimeChartComponent } from './charts/ingredients-dispense-time-chart/ingredients-dispense-time-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    HomeComponent,
    DisplayUserComponent,
    LoginUserComponent,
    SaladCountChartComponent,
    SaladTimeChartComponent,
    StatisticalTimeChartComponent,
    StatisticalIngredientChartComponent,
    StatisticalIngredientTimeChartComponent,
    StatsCardsComponent,
    IngredientsDispenseChartComponent,
    IngredientsWeightAccuracyChartComponent,
    IngredientTimeBubbleChartComponent,
    IngredientsDispenseTimeChartComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    ComponentsModule,
    RouterModule,
    AppRoutingModule,
    AlertModule.forRoot(),
    AngularFireModule.initializeApp(firebaseConfig, "chowbotics dashboard"),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    NgxChartsModule,
    BrowserAnimationsModule,
  ],
  providers: [AuthService, SaladsService, SaladDispensingDetailsService, DateService],
  bootstrap: [AppComponent]
})
export class AppModule { }
