import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaladTimeChartComponent } from './salad-time-chart.component';

describe('SaladTimeChartComponent', () => {
  let component: SaladTimeChartComponent;
  let fixture: ComponentFixture<SaladTimeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaladTimeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaladTimeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
