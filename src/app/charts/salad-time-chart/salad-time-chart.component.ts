import { Input, Component, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'app-salad-time-chart',
	templateUrl: './salad-time-chart.component.html',
	styleUrls: ['./salad-time-chart.component.scss']
})
export class SaladTimeChartComponent implements OnInit, OnChanges {

	@Input() saladsList: any[];

	instructionTimeContributionData: any[] = [];



	// Chart Options
	CHART_VIEW: any[] = [1080, 400];
	CHART_COLOR_SCHEME = {
		domain: ['#A4E666', '#31B96E', '#1B1112', '#616536', '#F2F39E', '#397249', '#C7E1BA']
	};

	instructionTypeNamesMap = {
		"DISPENSE": "Dispense",
		"CHANGE_CONTAINER": "Change Container",
		"STRAIGHTEN": "Straighten",
		"INSULATION_ON": "Insulation On",
		"DELAY": "Delay",
	}

	constructor() {

	}

	createCleanInstructionTypeTimeData() {
		let cleanInstructionTypeTimeData = {};
		for (let key in this.instructionTypeNamesMap) {
			cleanInstructionTypeTimeData[key] = 0;
		}
		return cleanInstructionTypeTimeData;
	}

	updateSaladTimeContributionChart() {

		let instructionTypeTimeData = this.createCleanInstructionTypeTimeData();

		Object.keys(this.saladsList).forEach(saladKey => {

			let salad = this.saladsList[saladKey];

			Object.keys(salad).forEach(codeKey => {

				let attempts: any[] = salad[codeKey];
				let attemptsTimeCount: number = 0;

				Object.keys(attempts).forEach(attemptId => {

					let steps: any[] = attempts[attemptId];
					let stepTimeCount: number = 0;

					Object.keys(steps).forEach(instructionKey => {
						let shouldSkip: boolean = false;

						let instruction: any[] = steps[instructionKey];
						let instructionTypeString: string = instruction['instructionType'];

						if (instructionTypeString == 'DISPENSE') {
							let ingredientQuantity: number = instruction['ingredientQuantity'];
							if (ingredientQuantity == null || ingredientQuantity < 0) {
								shouldSkip = true;
							} else {
								shouldSkip = false;
							}
						}

						if (!shouldSkip) {

							let actualTimeCount = instruction['actualTimeCount'];
							let instructionTimeCount = instruction['instructionTimeCount'];

							instructionTypeTimeData[instructionTypeString] = instructionTypeTimeData[instructionTypeString] + instructionTimeCount;

							if (actualTimeCount) {
								let valueDelay = instructionTypeTimeData[instructionTypeString];
								instructionTypeTimeData["DELAY"] = (valueDelay + (actualTimeCount - instructionTimeCount));
							}
						}
					});
				});
			});
		});

		let instructionTimeContributionDataObjectList = [];
		for (let key in instructionTypeTimeData) {
			let totalTime = instructionTypeTimeData[key] / 1000;
			instructionTimeContributionDataObjectList.push({
				"name": this.instructionTypeNamesMap[key],
				"value": (totalTime / this.saladsList.length)
			});
		}

		this.instructionTimeContributionData = instructionTimeContributionDataObjectList;
	}

	ngOnInit() {}

	ngOnChanges() {
		if (this.saladsList) {
			this.updateSaladTimeContributionChart();
		}
	}
}
