
// import {single, multi} from './data';
import {NgxChartsModule} from '@swimlane/ngx-charts';


import { Input, Component, OnInit } from '@angular/core';

import * as shape from 'd3-shape';
import * as d3 from 'd3';

import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Component({
  selector: 'app-statistical-time-chart',
  templateUrl: './statistical-time-chart.component.html',
  styleUrls: ['./statistical-time-chart.component.scss']
})

export class StatisticalTimeChartComponent implements OnInit {

  @Input() saladKey: string;

  items: FirebaseListObservable<any[]>;

  instructionTimeData: any[] = [];
  instructionTimeData2: any[] = [];
  single: any[] = [];

  
  showLabels = true;
  explodeSlices = false;
  doughnut = false;

  view: any[] = [320, 285];

  // options
  colorScheme = {
    domain: ['#A4E666', '#31B96E', '#397249', '#616536', '#1B1112', '#F2F39E', '#C7E1BA' ]
    //domain: ['#FF3333', '#FF33FF', '#CC33FF', '#0000FF', '#33CCFF', '#33FFFF', '#33FF66', '#CCFF33', '#FFCC00', '#FF6600']
  };

  constructor(public afAuth: AngularFireAuth, public af: AngularFireDatabase) {
    //Object.assign(this, {this.single});   

    const promise = af.list(('/redwood/salads/'), {
      preserveSnapshot: true,
      query: {
        limitToLast: 50
      }
    })
    .subscribe(salads => {

      let itd: any[] = [];
      let ittd: any[] = [];
      let itdMap = new Map<string,number>();
      let ittdMap = new Map<string,number>();

      salads.forEach( (salad:any) => {

        let saladTimeCount: number = 0;
        salad.forEach( (steps:any) => {

          let stepTimeCount: number = 0;
          steps.forEach( (instruction:any) => {

            let iString: string 
            iString = steps.key != null ? steps.key : 'UNDEFINED' ; 
            if( itdMap.get(iString) == null ){
              itdMap.set(iString, instruction.val().instructionTimeCount)
            } else {
              let temp: number = itdMap.get(iString);
              itdMap.set(iString, temp + instruction.val().instructionTimeCount)
            }
            // saladTimeCount = saladTimeCount + instruction.val().instructionTimeCount;
            
            let iTypeString: string 
            iTypeString = instruction.val().instructionType != null ? instruction.val().instructionType : 'INSULATION' ; 
            if( ittdMap.get(iTypeString) == null ){
              ittdMap.set(iTypeString, instruction.val().instructionTimeCount)
            } else {
              let temp: number = ittdMap.get(iTypeString);
              ittdMap.set(iTypeString, temp + instruction.val().instructionTimeCount)
            }

          });

        });

      });

      itdMap.forEach( (value: number, key: string) => {
        // itd.push({name:key, value:(value/1000)});
        itd.push({name:key, value:(value/1000/salads.length)});
      });

      ittdMap.forEach( (value: number, key: string) => {
        // ittd.push({name:key, value:(value/1000)});
        ittd.push({name:key, value:(value/1000/salads.length)});
      });

      itd.sort((a: any, b: any) => {
        if (a.value < b.value) {
          return 1;
        } else if (a.value > b.value) {
          return -1;
        } else {
          return 0;
        }
      });


      ittd.sort((a: any, b: any) => {
        if (a.value < b.value) {
          return 1;
        } else if (a.value > b.value) {
          return -1;
        } else {
          return 0;
        }
      });

      this.instructionTimeData = itd;
      this.instructionTimeData2 = ittd;

    });


  }
  

  ngOnInit() {
    this.single = [
      {
        "name": "Rotation",
        "value": 27
      },
      {
        "name": "Dispensing",
        "value": 48
      },
      {
        "name": "Selecting",
        "value": 131
      },
    ];
  }

}
