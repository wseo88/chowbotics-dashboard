import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticalTimeChartComponent } from './statistical-time-chart.component';

describe('StatisticalTimeChartComponent', () => {
  let component: StatisticalTimeChartComponent;
  let fixture: ComponentFixture<StatisticalTimeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticalTimeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticalTimeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
