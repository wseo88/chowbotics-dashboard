import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticalIngredientTimeChartComponent } from './statistical-ingredient-time-chart.component';

describe('StatisticalIngredientTimeChartComponent', () => {
  let component: StatisticalIngredientTimeChartComponent;
  let fixture: ComponentFixture<StatisticalIngredientTimeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticalIngredientTimeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticalIngredientTimeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
