import { Input, Component, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'app-statistical-ingredient-time-chart',
	templateUrl: './statistical-ingredient-time-chart.component.html',
	styleUrls: ['./statistical-ingredient-time-chart.component.scss']
})
export class StatisticalIngredientTimeChartComponent implements OnInit, OnChanges {

	@Input() saladList: any[];

	instructionTimeData: any[] = [];
	instructionTimeData2: any[] = [];
	ingredientWeightData: any[] = [];

	view: any[] = [1080, 500];

	// options
	colorScheme = {
		domain: ['transparent', '#A4E666', '#31B96E']
	};
	showXAxis = true;
	showYAxis = true;
	gradient = false;
	showLegend = false;
	showXAxisLabel = false;
	showYAxisLabel = true;

	yAxisLabel = 'Time to Dispense [sec]';
	xAxisLabel = 'Ingredient';

	constructor() {


	}

	ngOnChanges() {

		let itd: any[] = [];

		let itdMap = new Map<string,number>();
		let itdCountMap = new Map<string,number>();
		let itMinMap = new Map<string,number>();
		let itMaxMap = new Map<string,number>();

		// data comes in the form of:
		// {
		//		"salads": {
		// 			"<saladKey>": {
		// 				"<codeKey>": {
		//					"<attemptId>": {
		//						"<gcode command>": {
		//							"actualTimeCount": <value -int in ms>,
		//							"ingredientIndex": <value - int>,
		//							"ingredientQuantity": <value -int in g>,
		//							"ingredientQuantityTarget": <value - int in g>,
		//							"instructionCode": <value - String>,
		//							"instuctionTimeCount": <value - int in ms>,
		//							"instructionType": <value - string>,
		//							"message": <value - String>,
		//							"statusCode": <value - String>,
		//						}
		//					}
		//				}
		//			},
		//			...
		//	}


		if(this.saladList){

			Object.keys(this.saladList).forEach(saladKey=> {

				let salad: any[] = this.saladList[saladKey];

				Object.keys(salad).forEach(codeKey=> {

					let attempts: any[] = salad[codeKey];
					let totalTime = 0;

					Object.keys(attempts).forEach(attemptId=> {

						let steps: any[] = attempts[attemptId];

						Object.keys(steps).forEach(instructionKey=> {

							let instruction: any[] = steps[instructionKey];

							if('DISPENSE' == instruction['instructionType'] && 'SUCCESS' == instruction['statusCode']){
								let stepTime: number = instruction['instructionTimeCount'] / 1000;
								totalTime = totalTime + stepTime;
							}

						});

						if(totalTime > 0){

							let iString: string = codeKey != null ? codeKey : 'UNDEFINED' ;

							if( itdMap.get(iString) == null ){
								itdMap.set(iString, totalTime);
								itdCountMap.set(iString, 1);
							} else {
								let temp: number = itdMap.get(iString);
								itdMap.set(iString, temp + totalTime);
								itdCountMap.set(iString, itdCountMap.get(iString) + 1);
							}

							if(itMinMap.get(iString) == null){
								itMinMap.set(iString, totalTime);
							}else{
								if (totalTime < itMinMap.get(iString)){
									itMinMap.set(iString, totalTime);
								}
							}
							if(itMaxMap.get(iString) == null){
								itMaxMap.set(iString, totalTime);
							}else{
								if (totalTime > itMaxMap.get(iString)){
									itMaxMap.set(iString, totalTime);
								}
							}
							
						}
					});

				});
			});

			itdMap.forEach( (value: number, key: string) => {
				if('complete' != key){
					let min: number = itMinMap.get(key);
					let max: number = itMaxMap.get(key);
					let d: any = {
						'name':key, 
						'series':[
						{ 'name': 'min', 'value': (min) },
						{ 'name': 'max', 'value': (max-min > 0.5 ? max-min: 0.5) }
						]
					}
					itd.push(d);
				}
			});


			itd.sort((a: any, b: any) => {
				if (a.series[0].value > b.series[0].value) {
					return 1;
				} else if (a.series[0].value < b.series[0].value) {
					return -1;
				} else {
					return 0;
				}
			});

			this.ingredientWeightData = itd;

		}

	}

	ngOnInit() {
	}

}
