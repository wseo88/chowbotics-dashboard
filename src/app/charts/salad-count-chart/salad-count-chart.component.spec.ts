import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SaladTotalComponent } from './statistical-info-chart.component';

describe('SaladTotalComponent', () => {
  let component: SaladTotalComponent;
  let fixture: ComponentFixture<SaladTotalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SaladTotalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SaladTotalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
