
// import {single, multi} from './data';
import {NgxChartsModule} from '@swimlane/ngx-charts';


import { Input, Component, OnInit, OnChanges } from '@angular/core';
import { SaladsService } from 'app/shared/salads.service'

import * as shape from 'd3-shape';
import * as d3 from 'd3';

@Component({
  selector: 'app-salad-count-chart',
  templateUrl: './salad-count-chart.component.html',
  styleUrls: ['./salad-count-chart.component.scss']
})

export class SaladCountChartComponent implements OnInit, OnChanges {

  @Input() saladsList: any[];

  instructionTimeData: any[] = [];
  instructionTimeData2: any[] = [];
  single: any[] = [];

  view: any[] = [1080, 250];
  view2: any[] = [1000, 500];

  // options
  colorScheme = {
    domain: ['#A4E666', '#31B96E', '#1B1112', '#616536', '#F2F39E', '#397249', '#C7E1BA' ]
  };
  black = '#DDDDDD';


  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = '';
  showYAxisLabel = false;
  yAxisLabel = '';

  constructor(private saladsService: SaladsService) {
    
  }


  ngOnInit() {
  }

  ngOnChanges() {

    //console.log(this.getLastSevenDays());
    // data comes in the form of:
    // {
    //     "<saladKey>": {
    //       "saladDate": <saladDate>,
    //       "name": <saladName>,
    //       "custom" <custom - bool>
    //      },
    //      ...
    //  }

    // need to output in the form of:
    //
    // [
    //   {
    //     "name": <dateString>,
    //     "series": [
    //                  {"name": <saladName>, "value": <count>}
    //               ]
    //   }
    // ]

    if (this.saladsList) {

      let saladsData = this.saladsService.getFlattenedData();

      let results = {};

      Object.keys(saladsData).forEach(function(key) {
        let salad: any[] = saladsData[key];
        let saladDate = salad['saladDate'];
        let saladName = salad['name'];
        let saladDateString = (saladDate.getMonth()+1) + '/' + saladDate.getDate() + '/17';
        if (results[saladDateString] != null){
          let temp = results[saladDateString];
          if (temp[saladName] != null){
            temp[saladName] = temp[saladName] + 1;
          } else {
            temp[saladName] = 1;
          }
          results[saladDateString] = temp;
        } else {
          let temp = {};
          temp[saladName] = 1;
          results[saladDateString] = temp;
        }
      });

      let saladCreationData = [];
      Object.keys(results).forEach(function(saladDate) {
        let temp = results[saladDate];
        let series = [];
        Object.keys(temp).forEach(function(nameData) {
          series.push({"name": nameData, "value": temp[nameData]});
        });
        saladCreationData.push({"name":saladDate, "series": series});
      });
      //console.log(saladCreationData);
      this.single = saladCreationData;
    } else {
      //console.log('noe!');
    }
  }
}
