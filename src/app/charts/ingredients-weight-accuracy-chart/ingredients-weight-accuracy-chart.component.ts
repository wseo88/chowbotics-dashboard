import { Input, Component, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'app-ingredients-weight-accuracy-chart',
	templateUrl: './ingredients-weight-accuracy-chart.component.html',
	styleUrls: ['./ingredients-weight-accuracy-chart.component.scss']
})
export class IngredientsWeightAccuracyChartComponent implements OnInit, OnChanges {
	@Input() saladsList: any[];

	ingredientWeightAccuracyData: any[] = [];
	isStdDev: boolean = false;

	// Chart Options
	CHART_VIEW: any[] = [1080, 400];
	CHART_COLOR_SCHEME = {
		domain: ['#A4E666', '#31B96E', '#1B1112', '#616536', '#F2F39E', '#397249', '#C7E1BA']
	};

	showXAxis = true;
	showYAxis = true;
	gradient = false;
	showLegend = false;
	showXAxisLabel = false;
	showYAxisLabel = true;
	yAxisLabel = 'Weight Accuracy [% of Target]';
	xAxisLabel = 'Ingredients';

	ingredientNamesMap = {
		"bacon":                    "Bacon",
		"barley_corn_salad":        "Barley Corn",
		"basil_citrus_vinaigrette": "Basil Citrus",
		"blue_cheese":              "Blue Cheese",
		"cherry_tomato":            "Cherry Tomatoes",
		"cranberries":              "Cranberries",
		"croutons":                 "Croutons",
		"feta":                     "Feta Cheese",
		"kale":                     "Kale",
		"lemon":                    "Lemon",
		"lentil_cucumber_salad":    "Lentil Cucumber",
		"marinated_tofu":           "Marinated Tofu",
		"mixed_greens":             "Mixed Greens",
		"quinoa":                   "Quinoa",
		"ranch":                    "Ranch",
		"roasted_yams":             "Roasted Yams",
		"romaine":                  "Romaine",
		"turkey":                   "Turkey",
		"walnuts":                  "Walnuts"
	};

	constructor() {

	}

	createCleanSaladAttemptData() {
		let cleanSaladAttemptData = {};
		for (let key in this.ingredientNamesMap) {
			cleanSaladAttemptData[key] = [];
		}
		return cleanSaladAttemptData;
	}

	private updateIngredientWeightAccuracyData() {
		let saladAttemptData = this.createCleanSaladAttemptData();

		Object.keys(this.saladsList).forEach(saladKey => {
			let salad = this.saladsList[saladKey];

			Object.keys(salad).forEach(commandKey => {
				if (commandKey == 'complete') {
					// Do not store 'complete' command code data
				} else {
					saladAttemptData[commandKey].push(salad[commandKey]);
				}
			});
		});
		return saladAttemptData;
	}

	private updateIngredientWeightAccuracyChart() {
		let	saladAttemptData = this.updateIngredientWeightAccuracyData();
		let ingredientsWeightAccuracyDataObject: any[] = [];

		for (let key in saladAttemptData) {

			let weightQuantityTotal = 0;
			let weightQuantitiyTarget = 0;
			let occurance = 0;

			let weightAccuracyAvg: number = 0;
			let weightAccuracyMin: number = null;
			let weightAccuracyMax: number = null;

			let weight: number = 0;
			let attempts = saladAttemptData[key];

			for (let i = 0; i < attempts.length; i++) {

				let steps = attempts[i];

				Object.keys(steps).forEach(stepKey => {

					let instructions: any[] = steps[stepKey];

					Object.keys(instructions).forEach(instructionKey => {

						let instruction = instructions[instructionKey]

						if (instruction['instructionType'] == 'DISPENSE' && instruction['statusCode'] == 'SUCCESS' && instruction['message'] != '' && instruction['ingredientQuantity'] != null) {

							let ingredientQuantity: number = instruction['ingredientQuantity'];
							let ingredientQuantityTarget: number = instruction['ingredientQuantityTarget'];

							let weightAccuracyPercentage = ingredientQuantity / ingredientQuantityTarget * 100;
							weightQuantityTotal += ingredientQuantity;

							if (weightAccuracyPercentage > 0) {
								if (weightAccuracyMin == null) {
									weightAccuracyMin = weightAccuracyPercentage
								} else {
									if (weightAccuracyPercentage < weightAccuracyMin) {
										weightAccuracyMin = weightAccuracyPercentage;
									}
								}
							}

							if (weightAccuracyPercentage < 200) {
								if (weightAccuracyMax == null) {
									weightAccuracyMax = weightAccuracyPercentage;
								} else {
									if (weightAccuracyPercentage > weightAccuracyMax) {
										weightAccuracyMax = weightAccuracyPercentage;
									}
								}
							}

							weightQuantitiyTarget += ingredientQuantityTarget;
							occurance++;
						}
					});
				});
			}

			weightAccuracyAvg = (weightQuantityTotal / weightQuantitiyTarget) * 100;
			weightAccuracyMin = weightAccuracyMin;
			weightAccuracyMax = weightAccuracyMax;

			let ingredientWeightAccuracyObject = {
				"name": this.ingredientNamesMap[key],
				"series": [
					{
						"name": "Avg",
						"value": weightAccuracyAvg 
					}
				]
			}

			ingredientsWeightAccuracyDataObject.push(ingredientWeightAccuracyObject);
		}

		this.ingredientWeightAccuracyData = ingredientsWeightAccuracyDataObject;

		// sort by ingredient name
		this.ingredientWeightAccuracyData.sort((a: any, b: any) => {
	      if (a['name'] < b['name']) {
	        return -1;
    	  } else if (a['name'] > b['name']) {
        	return 1;
	      } else {
    	    return 0;
      	  }
    	});
	}

	ngOnInit() {

	}

	ngOnChanges() {
		if (this.saladsList) {
			this.updateIngredientWeightAccuracyChart();
		}
	}
}
