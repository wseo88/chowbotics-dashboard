import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientsWeightAccuracyChartComponent } from './ingredients-weight-accuracy-chart.component';

describe('IngredientsWeightAccuracyChartComponent', () => {
  let component: IngredientsWeightAccuracyChartComponent;
  let fixture: ComponentFixture<IngredientsWeightAccuracyChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientsWeightAccuracyChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientsWeightAccuracyChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
