import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientTimeBubbleChartComponent } from './ingredient-time-bubble-chart.component';

describe('IngredientTimeBubbleChartComponent', () => {
  let component: IngredientTimeBubbleChartComponent;
  let fixture: ComponentFixture<IngredientTimeBubbleChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientTimeBubbleChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientTimeBubbleChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
