import { Input, Component, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'app-ingredient-time-bubble-chart',
	templateUrl: './ingredient-time-bubble-chart.component.html',
	styleUrls: ['./ingredient-time-bubble-chart.component.scss']
})
export class IngredientTimeBubbleChartComponent implements OnInit, OnChanges {

	@Input() saladList: any[];
	@Input() currentIngredient: string;

	ingredientList: string[] = [];
	ingredientWeightData: any[] = [];
	iwdMap: Map<string,Map<string,any[]>> = new Map<string,any>();


	view: any[] = [1000, 400];

	// options
	colorScheme = {
		domain: ['#A4E666', '#31B96E', '#1B1112', '#616536', '#F2F39E', '#397249', '#C7E1BA', '#333000']
	};
	showXAxis = true;
	showYAxis = true;
	gradient = false;
	showLegend = false;
	showXAxisLabel = true;
	showYAxisLabel = true;

	yAxisLabel = 'Weight Dispensed [g]';
	xAxisLabel = 'Time to Dispense [sec]';

	constructor() {


	}

	
	public selectIngredient(ingredient:string) {

		this.currentIngredient = ingredient;
		let iwd: any[] = [];

		this.iwdMap.forEach( (section: Map<string,any[]>, key: string) => {
			if(this.currentIngredient == key){


				section.forEach( (algo: any[], algoKey: string) => {

					let series: any[] = [];

					for (var data of algo){
						let d: any = { 
							'name': '', 
							'y': data['weight'],
							'x': data['time'],
							'r': '10'
						};
						series.push(d);
					}


					let dots: any = {'name':algoKey,series:series};
					iwd.push(dots);


				});
			}
		});

		this.ingredientWeightData = iwd;
		console.log(this.ingredientWeightData);
	}

	ngOnChanges() {


		// data comes in the form of:
		// {
		//		"salads": {
		// 			"<saladKey>": {
		// 				"<codeKey>": {
		//					"<attemptId>": {
		//						"<gcode command>": {
		//							"actualTimeCount": <value -int in ms>,
		//							"ingredientIndex": <value - int>,
		//							"ingredientQuantity": <value -int in g>,
		//							"ingredientQuantityTarget": <value - int in g>,
		//							"instructionCode": <value - String>,
		//							"instuctionTimeCount": <value - int in ms>,
		//							"instructionType": <value - string>,
		//							"message": <value - String>,
		//							"statusCode": <value - String>,
		//						}
		//					}
		//				}
		//			},
		//			...
		//	}


		if(this.saladList){


			Object.keys(this.saladList).forEach(saladKey=> {

				let salad: any[] = this.saladList[saladKey];

				Object.keys(salad).forEach(codeKey=> {

					let attempts: any[] = salad[codeKey];

					Object.keys(attempts).forEach(attemptId=> {

						let steps: any[] = attempts[attemptId];

						Object.keys(steps).forEach(instructionKey=> {

							let instruction: any[] = steps[instructionKey];

							if('DISPENSE' == instruction['instructionType'] && 'SUCCESS' == instruction['statusCode'] && instruction['ingredientQuantityTarget'] > 0){

								let stepTime: number = instruction['instructionTimeCount'] / 1000;
								let iString: string = codeKey != null ? codeKey : 'UNDEFINED' ;

								let msg: string = instruction['message'];

								let section: string[] = msg.split(',');
								let count:number = 1;

								for (var val of section){

									let weight: number = 0;
									if (val != '' && val != null){
										weight = +val;
									}else if (instruction['ingredientQuantity'] != null && instruction['ingredientQuantity'] != ''){
										weight = +instruction['ingredientQuantity']
									}

									let time: number = count / (section.length) * stepTime;

									if( this.iwdMap.get(iString) == null ){
										this.ingredientList.push(iString);
										this.iwdMap.set(iString, new Map<string,any[]>());
									}

									let algoMap: Map<string,any[]> = this.iwdMap.get(iString);
									if(algoMap.get(instructionKey) == null){
										algoMap.set(instructionKey,[]);
									}

									let temp: any[] = algoMap.get(instructionKey);
									temp.push({"weight":weight,"time":time});
									algoMap.set(instructionKey,temp);

									this.iwdMap.set(iString, algoMap);

									count = count + 1;

								}

							}


						});
					});
				});
			});
		}
	}

	ngOnInit() {
	}

}
