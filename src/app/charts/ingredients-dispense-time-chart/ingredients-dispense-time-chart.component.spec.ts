import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientsDispenseTimeChartComponent } from './ingredients-dispense-time-chart.component';

describe('IngredientsDispenseTimeChartComponent', () => {
  let component: IngredientsDispenseTimeChartComponent;
  let fixture: ComponentFixture<IngredientsDispenseTimeChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientsDispenseTimeChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientsDispenseTimeChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
