import { Input, Component, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'app-ingredients-dispense-time-chart',
	templateUrl: './ingredients-dispense-time-chart.component.html',
	styleUrls: ['./ingredients-dispense-time-chart.component.scss']
})

export class IngredientsDispenseTimeChartComponent implements OnInit, OnChanges {
	@Input() saladsList:any[];

	saladsListDataFiltered = {};
	ingredientsDispenseTimeData: any[] = [];
	
	ingredientNamesMap = {
		"bacon":                    "Bacon",
		"barley_corn_salad":        "Barley Corn",
		"basil_citrus_vinaigrette": "Basil Citrus",
		"blue_cheese":              "Blue Cheese",
		"cherry_tomato":            "Cherry Tomatoes",
		"cranberries":              "Cranberries",
		"croutons":                 "Croutons",
		"feta":                     "Feta Cheese",
		"kale":                     "Kale",
		"lemon":                    "Lemon",
		"lentil_cucumber_salad":    "Lentil Cucumber",
		"marinated_tofu":           "Marinated Tofu",
		"mixed_greens":             "Mixed Greens",
		"quinoa":                   "Quinoa",
		"ranch":                    "Ranch",
		"roasted_yams":             "Roasted Yams",
		"romaine":                  "Romaine",
		"turkey":                   "Turkey",
		"walnuts":                  "Walnuts"
	};

	// Chart Options
	CHART_VIEW: any[] = [1080, 400];
	CHART_COLOR_SCHEME = {
		domain: ['#A4E666', '#31B96E', '#1B1112', '#616536', '#F2F39E', '#397249', '#C7E1BA', '#333000']
	};
	showXAxis = true;
	showYAxis = true;
	gradient = false;
	showLegend = false;
	showXAxisLabel = true;
	showYAxisLabel = true;
	yAxisLabel = 'Time [sec]';
	xAxisLabel = 'Ingredients';

	constructor() {

	}

	private filterSaladsListData() {
		let saladsListDataFiltered = {};

		Object.keys(this.saladsList).forEach(saladKey => {
			let salad = this.saladsList[saladKey];
			Object.keys(salad).forEach(commandKey => {

				if (commandKey == 'complete') {
					// Do not store 'complete' command code data
				} else {
					if (commandKey in saladsListDataFiltered) {
						// Do nothing
					} else {
						saladsListDataFiltered[commandKey] = [];
					}
					saladsListDataFiltered[commandKey].push(salad[commandKey]);
				}
			});
		});
		this.saladsListDataFiltered = saladsListDataFiltered;
		return saladsListDataFiltered;
	}

	private displayIngredientsDispenseTimeChart() {

		let ingredientsDispenseTimeDataList = [];
		let saladsListDataFiltered = this.filterSaladsListData();

		for (let key in saladsListDataFiltered) {

			let ingredientsDispenseTimeActual = 0;
			let ingredientsDispenseTimeInstruction = 0;

			let attempts = saladsListDataFiltered[key];

			for (let i = 0; i < attempts.length; i++) {

				let steps = attempts[i];

				Object.keys(steps).forEach(stepKey => {

					let instructions: any[] = steps[stepKey];

					Object.keys(instructions).forEach(instructionKey => {

						let instruction = instructions[instructionKey]

						if (instruction['instructionType'] == 'DISPENSE' && instruction['statusCode'] == 'SUCCESS' && instruction['ingredientQuantityTarget'] > 0 && instruction['ingredientQuantity'] != null) {

							let actualTimeCount = instruction['actualTimeCount'];
							let instructionTimeCount = instruction['instructionTimeCount'];
							let delayTimeCount = actualTimeCount - instructionTimeCount;

							ingredientsDispenseTimeActual += actualTimeCount;
							ingredientsDispenseTimeInstruction += instructionTimeCount;
						}
					});
				});
			}

			let ingredientsdispensetimedata: any = {
				'name': this.ingredientNamesMap[key],
				'value': (ingredientsDispenseTimeActual / attempts.length) / 1000
			};
			ingredientsDispenseTimeDataList.push(ingredientsdispensetimedata);
		}

		this.ingredientsDispenseTimeData = ingredientsDispenseTimeDataList;

		// sort by ingredient name
		this.ingredientsDispenseTimeData.sort((a: any, b: any) => {
	      if (a['name'] < b['name']) {
	        return -1;
    	  } else if (a['name'] > b['name']) {
        	return 1;
	      } else {
    	    return 0;
      	  }
    	});

	}

	ngOnChanges() {
		if (this.saladsList) {
			this.displayIngredientsDispenseTimeChart();
		}
	}

	ngOnInit() {

	}
}
