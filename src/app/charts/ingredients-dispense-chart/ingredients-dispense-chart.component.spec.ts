import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientsDispenseChartComponent } from './ingredients-dispense-chart.component';

describe('IngredientsDispenseChartComponent', () => {
  let component: IngredientsDispenseChartComponent;
  let fixture: ComponentFixture<IngredientsDispenseChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngredientsDispenseChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientsDispenseChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
