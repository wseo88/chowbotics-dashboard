import { Input, Component, OnInit, OnChanges } from '@angular/core';
import { DateService } from 'app/shared/date.service';

@Component({
  selector: 'app-ingredients-dispense-chart',
  templateUrl: './ingredients-dispense-chart.component.html',
  styleUrls: ['./ingredients-dispense-chart.component.scss']
})

export class IngredientsDispenseChartComponent implements OnInit, OnChanges {

	@Input() saladsList: any[];

	ingredientsDispenseData: any[] = [];

	today;
	dates = [];

	// Chart Options
	CHART_VIEW: any[] = [1080, 400];
	CHART_COLOR_SCHEME = {
		domain: ['#A4E666', '#31B96E', '#1B1112', '#616536', '#F2F39E', '#397249', '#C7E1BA']
	};

	showXAxis = true;
	showYAxis = true;
	gradient = false;
	showLegend = false;
	showXAxisLabel = false;
	showYAxisLabel = true;
	yAxisLabel = 'Weight(g)';
	xAxisLabel = 'Ingredients';

	ingredientNamesMap = {
		"bacon":                    "Bacon",
		"barley_corn_salad":        "Barley Corn",
		"basil_citrus_vinaigrette": "Basil Citrus",
		"blue_cheese":              "Blue Cheese",
		"cherry_tomato":            "Cherry Tomatoes",
		"cranberries":              "Cranberries",
		"croutons":                 "Croutons",
		"feta":                     "Feta Cheese",
		"kale":                     "Kale",
		"lemon":                    "Lemon",
		"lentil_cucumber_salad":    "Lentil Cucumber",
		"marinated_tofu":           "Marinated Tofu",
		"mixed_greens":             "Mixed Greens",
		"quinoa":                   "Quinoa",
		"ranch":                    "Ranch",
		"roasted_yams":             "Roasted Yams",
		"romaine":                  "Romaine",
		"turkey":                   "Turkey",
		"walnuts":                  "Walnuts"
	};

	constructor(private dateService: DateService) {
		this.today = dateService.today();
		for (let i = 0; i < 14; i++) {
			this.dates.push(this.dateService.getDaysAgo(i));
		}
	}

	createCleanSaladAttemptData() {
		let cleanSaladAttemptData = {};
		for (let key in this.ingredientNamesMap) {
			cleanSaladAttemptData[key] = [];
		}
		return cleanSaladAttemptData;
	}

	updateIngredientsDispenseData(date) {
		let selectedDate = new Date(date);
		let selectedDateNext = this.dateService.getNextDay(selectedDate);

		let saladAttemptData = this.createCleanSaladAttemptData();

		Object.keys(this.saladsList).forEach(saladKey => {
			let salad = this.saladsList[saladKey];
        	let timestamp: Date = this.dateService.convertDateStringToDate(salad.$key);

        	if (timestamp >= selectedDate && timestamp < selectedDateNext) {
				Object.keys(salad).forEach(commandKey => {
					if (commandKey == 'complete') {
						// Do not store 'complete' command code data
					} else {
						let attemptData: any[] = salad[commandKey];
						if (commandKey in saladAttemptData) {
							saladAttemptData[commandKey].push(attemptData);
						} else {
							saladAttemptData[commandKey] = [
								attemptData
							];
						}
					}
				});
			}
		});
		return saladAttemptData;
	}

	updateIngredientsDispenseChart(date) {

		let saladAttemptData = this.updateIngredientsDispenseData(date);
		let ingredientsDispenseDataObject: any[] = [];

		for (let key in saladAttemptData) {

			let weight: number = 0;
			let attempts = saladAttemptData[key];

			for (let i = 0; i < attempts.length; i++) {
				let attempt = attempts[i];
				Object.keys(attempt).forEach(attemptId => {

					let instructions: any[] = attempt[attemptId];
					let stepTimeCount: number = 0;

					Object.keys(instructions).forEach(instructionKey => {

						let instruction: any[] = instructions[instructionKey];
						let ingredientQuantity = instruction['ingredientQuantityTarget'];

						if (ingredientQuantity != null && !isNaN(ingredientQuantity)) {
							weight += ingredientQuantity;
						} else {
							weight += 0;
						}

					});
				});
			}

			let dispenseData: any = {
				'name': this.ingredientNamesMap[key],
				'value': weight
			};
			ingredientsDispenseDataObject.push(dispenseData);
		}

		this.ingredientsDispenseData = ingredientsDispenseDataObject;

		// sort by ingredient name
		this.ingredientsDispenseData.sort((a: any, b: any) => {
	      if (a['name'] < b['name']) {
	        return -1;
    	  } else if (a['name'] > b['name']) {
        	return 1;
	      } else {
    	    return 0;
      	  }
    	});
	}

	ngOnInit() {

	}

	ngOnChanges() {
		if (this.saladsList) {
			this.updateIngredientsDispenseChart(this.today);
		}
	}
}
