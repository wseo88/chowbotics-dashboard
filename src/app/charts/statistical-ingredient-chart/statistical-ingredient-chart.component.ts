import { Input, Component, OnInit, OnChanges } from '@angular/core';

@Component({
	selector: 'app-statistical-ingredient-chart',
	templateUrl: './statistical-ingredient-chart.component.html',
	styleUrls: ['./statistical-ingredient-chart.component.scss']
})
export class StatisticalIngredientChartComponent implements OnInit, OnChanges {

	@Input() saladList: any[];

	instructionTimeData: any[] = [];
	instructionTimeData2: any[] = [];
	ingredientWeightData: any[] = [];

	isStdDev: boolean = false;

	view: any[] = [1080, 400];

	// options
	colorScheme = {
		domain: ['transparent', '#A4E666', '#31B96E', '#36B96E', '#A4E666',]
	};
	  showXAxis = true;
	  showYAxis = true;
	  gradient = false;
	  showLegend = false;
	  showXAxisLabel = false;
	  showYAxisLabel = true;

	  yAxisLabel = 'Weight Accuracy [% of Target]';
	  xAxisLabel = 'Ingredient';

	constructor() {}

	public toggleStdDev(){
		this.isStdDev = !this.isStdDev;
		this.calcChart();
	}

	private calcChart() {

		let iwd: any[] = [];

		let iwtdMap = new Map<string,number>();
		let iwdMap = new Map<string,number>();
		let iwAvgMap = new Map<string,number>();
		let iwAvgCountMap = new Map<string,number>();
		let iwStdDevMap = new Map<string,number>();
		let iwStdDevCountMap = new Map<string,number>();
		let iwMinMap = new Map<string,number>();
		let iwMaxMap = new Map<string,number>();

		    // data comes in the form of:
		    // {
		    //    "salads": {
		    //       "<saladKey>": {
		    //         "<codeKey>": {
		    //          "<attemptId>": {
		    //            "<gcode command>": {
		    //              "actualTimeCount": <value -int in ms>,
		    //              "ingredientIndex": <value - int>,
		    //              "ingredientQuantity": <value -int in g>,
		    //              "ingredientQuantityTarget": <value - int in g>,
		    //              "instructionCode": <value - String>,
		    //              "instuctionTimeCount": <value - int in ms>,
		    //              "instructionType": <value - string>,
		    //              "message": <value - String>,
		    //              "statusCode": <value - String>,
		    //            }
		    //          }
		    //        }
		    //      },
		    //      ...
		    //  }


	    if(this.saladList){

	      Object.keys(this.saladList).forEach(saladKey=> {

	        let salad: any[] = this.saladList[saladKey];

				Object.keys(salad).forEach(codeKey=> {

					let attempts: any[] = salad[codeKey];

					Object.keys(attempts).forEach(attemptKey=> {

						let steps: any[] = attempts[attemptKey];

						Object.keys(steps).forEach(instructionKey=> {

							let instruction: any[] = steps[instructionKey];

							if('SUCCESS' == instruction['statusCode'] && '' != instruction['message']){

								let iString: string = codeKey != null ? codeKey : 'UNDEFINED' ; 
								let target: number = instruction['ingredientQuantityTarget'];
								let actual: number = instruction['ingredientQuantity'];

								if( iwtdMap.get(iString) == null ){
									iwtdMap.set(iString, target);
								} else {
									let temp: number = iwtdMap.get(iString);
									iwtdMap.set(iString, temp + target);
								}

								if( iwdMap.get(iString) == null ){
									iwdMap.set(iString, actual);
								} else {
									let temp: number = iwdMap.get(iString);
									iwdMap.set(iString, temp + actual);
								}

								if( target > 0 && '' != instruction['message']){
									let percent = 100*actual/target;
									if(iwMinMap.get(iString) == null){
										iwMinMap.set(iString, percent);
									}else{
										if (percent < iwMinMap.get(iString)){
											iwMinMap.set(iString, percent);
										}
									}
									if(iwMaxMap.get(iString) == null){
										iwMaxMap.set(iString, percent);
									}else{
										if (percent > iwMaxMap.get(iString)){
											iwMaxMap.set(iString, percent);
										}
									}
									if(iwAvgMap.get(iString) == null){
										iwAvgMap.set(iString, percent);
										iwAvgCountMap.set(iString, 1);
									}else{
										iwAvgMap.set(iString, iwAvgMap.get(iString) + percent);
										iwAvgCountMap.set(iString, iwAvgCountMap.get(iString) + 1);
									}
								}

							}

						});
					});
				});
			});

	      Object.keys(this.saladList).forEach(saladKey=> {

	        let salad: any[] = this.saladList[saladKey];

				Object.keys(salad).forEach(codeKey=> {

					let steps: any[] = salad[codeKey];

					Object.keys(steps).forEach(instructionKey=> {

						let instruction: any[] = steps[instructionKey];

						if('SUCCESS' == instruction['statusCode'] && '' != instruction['message']){

							let iString: string = codeKey != null ? codeKey : 'UNDEFINED' ; 
							let target: number = instruction['ingredientQuantityTarget'];
							let actual: number = instruction['ingredientQuantity'];
							let avg: number =  iwAvgMap.get(iString)/iwAvgCountMap.get(iString);


							if( target > 0 && '' != instruction['message']){
								let percent = 100*actual/target;
								let percentVar = (percent - avg) * (percent - avg);

								if(iwStdDevMap.get(iString) == null){
									iwStdDevMap.set(iString, percentVar);
									iwStdDevCountMap.set(iString, 1);
								}else{
									iwStdDevMap.set(iString, iwStdDevMap.get(iString) + percentVar);
									iwStdDevCountMap.set(iString, iwStdDevCountMap.get(iString) + 1);
								}
							}

						}

					});
				});
			});	      


			iwdMap.forEach( (value: number, key: string) => {
				if('complete' != key){

					let average: number = iwAvgMap.get(key)/iwAvgCountMap.get(key);
					let stdev: number = Math.sqrt((iwStdDevMap.get(key)/iwStdDevCountMap.get(key)));
					let min: number = this.isStdDev ? average - 3*stdev : iwMinMap.get(key);
					let max: number = this.isStdDev ? average + 3*stdev : iwMaxMap.get(key);;

					let d: any = {
								'name':key, 
								'series':[
									{ 'name': 'min', 'value': (min) },
									{ 'name': 'average', 'value': (average - min) },
									{ 'name': 'max', 'value': (max - average) }
								]
							}
					iwd.push(d);
				}
			});

			iwd.sort((a: any, b: any) => {
				if (a.series[2].value+a.series[1].value > b.series[2].value+b.series[1].value) {
					return 1;
				} else if (a.series[2].value+a.series[1].value < b.series[2].value+b.series[1].value) {
					return -1;
				} else {
					return 0;
				}
			});

			this.ingredientWeightData = iwd;

		}

	}

	ngOnChanges() {
		this.calcChart();
	}

	ngOnInit() {
	}

}
