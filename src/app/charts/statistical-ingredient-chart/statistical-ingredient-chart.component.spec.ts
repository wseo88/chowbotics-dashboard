import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticalIngredientChartComponent } from './statistical-ingredient-chart.component';

describe('StatisticalIngredientChartComponent', () => {
  let component: StatisticalIngredientChartComponent;
  let fixture: ComponentFixture<StatisticalIngredientChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticalIngredientChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticalIngredientChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
